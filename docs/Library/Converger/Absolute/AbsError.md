# AbsError

Absolute Error

This converger tests the following ratio,

$$
\left|\mathbf{E}\right|\leqslant{}tol.
$$

where $$\mathbf{E}$$ denotes the error vector set by other solvers, etc..