# RelDisp

Relative Displacement

In the governing equation,

$$
\mathbf{K\Delta{}U=R},
$$

this converger tests the following ratio,

$$
\left|\mathbf{\dfrac{\Delta{}U}{U}}\right|\leqslant{}tol.
$$

where $$\mathbf{\Delta{}U}$$ denotes the displacement increment of the current sub-step and $$\mathbf{U}$$ denotes the
total displacement of the system.